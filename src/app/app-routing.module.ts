import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LogincomponentComponent} from './logincomponent/logincomponent.component';
import {AdminComponentComponent} from './admin-component/admin-component.component';
import {UserComponentComponent} from './user-component/user-component.component';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  { path: '', component: LogincomponentComponent },
  {path: 'admin', component: AdminComponentComponent},
  {path: 'user', component: UserComponentComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule { }
