import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-logincomponent',
  templateUrl: './logincomponent.component.html',
  styleUrls: ['./logincomponent.component.css']
})
export class LogincomponentComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router) {
  }

  model = {
    username: '',
    password: ''
  };

  ngOnInit(): void {
  }

  getauth() {

    if (this.model.username === 'admin@example.com') {
      this.router.navigate(['/admin']);
    } else {
      this.router.navigate(['/user']);
    }
  }
}
